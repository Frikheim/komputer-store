//Html elements used bye the javascript
const balanceElement = document.getElementById("balance");
const loanElement = document.getElementById("loan");
const payElement = document.getElementById("pay");
const pcListElement = document.getElementById("pcList");
const featuresElement = document.getElementById("features");
const infoElement = document.getElementById("info");
const priceElement = document.getElementById("price");
const imageElement = document.getElementById("image");
const loanButtonElement = document.getElementById("loanBtn");
const loanLabelElement = document.getElementById("loanLabel");
const nameElement = document.getElementById("name");
const repayElement = document.getElementById("repayLoanBtn");
let balance = 200;
let loan = 0;
let pay = 0;
let pcData = [];
let price = 0;
let boughtKomputer = false;

//fetches info about computers, stores in pcData variable and uses fuction to update the page
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => pcData = data)
    .then((pcData) => updateList(pcData));


//hjemme og vise repay loan

const updateList = (pcData) => {
    //adds each of the computers as choices in the selection tag with a function
    pcData.forEach(pc => addPc(pc));
    //show details for the default choice
    updateSpecs(pcData[0]);
    updateInfo(pcData[0]);
    updatePrice(pcData[0]);
    updateImage(pcData[0]);
    updateName(pcData[0]);
}
//adds a pc to the dropdown list
const addPc = (pc) => {
    //creates a option element
    const pcElement = document.createElement("option");
    //sets the value as the id
    pcElement.value = pc.id;
    //adds a textnode with the computer title to the element
    pcElement.appendChild(document.createTextNode(pc.title));
    //Adds the option element into the selection element
    pcListElement.appendChild(pcElement);
}

//function that gets the index for the chosen pc and updates the page with the correct info
const selectPC = choice => {
    const selectedPC = pcData[choice.target.selectedIndex];
    //calls individual functions for updating the page
    updateSpecs(selectedPC);
    updateInfo(selectedPC); 
    updatePrice(selectedPC); 
    updateImage(selectedPC);
    updateName(selectedPC); 
}
//adds a eventlistener to the dropdownlist
pcListElement.addEventListener("change",selectPC);
//function for updating the features of the pc
const updateSpecs = (pc) => {
    let text = ""; 
    //adds each line of specs to a variable
    pc.specs.forEach(spec => {
        text += spec + "\n";
    })
    //sets the tekst to the new features
    featuresElement.innerText = text;
}
//function that updates the description of the pc
const updateInfo = (pc) => {
    infoElement.innerText = pc.description;
}
//function that updates the price for the pc
const updatePrice = (pc) => {
    //updates the internal price variable
    price = pc.price;
    priceElement.innerText = price + "NOK";
}

//function that changes the source of the image to the chosen pc
const updateImage = (pc) => {
    imageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + pc.image;
}
//function that updates the name of the pc
const updateName = (pc) => {
    nameElement.innerText = pc.title;
}

//method that handles cliking on the get a loan button
function takeALoan() {
    //gives the user a promt for loan amount
    let loanSum = +prompt("please input loan amount");
    //checks if the loan amount is valid based on the balance in the bank
    if(loanSum <= balance*2) {
        //changes the internal balance and loan amounts
        balance += loanSum;
        loan += loanSum;
        //Updates the html elements with the new values
        let newBalance = balance+"kr";
        let newLoan = loanSum + "kr";
        balanceElement.innerText =newBalance;
        loanElement.innerText = newLoan;
        //Shows the repay loan button and information about loan amount to the user
        loanElement.style.display = "inline";
        repayElement.style.display = "block";
        loanLabelElement.style.display = "inline";
        //hides the loan button
        loanButtonElement.style.display = "none";
    }
    else {
        //alerts the user that the amount is too high
        alert("You dont have enough money to get this loan")
    }    
}
//function for when the work button is pressed
function work() {
    //updates internal pay variable
    pay += 100;
    //updates pay in html 
    payElement.innerText = pay + "kr";
}
//functiom for when the bank button is pressed
function bank() {
    //checks if the user has a loan
    if(loan > 0) {
        //calculates deduction and removes i from pay
        let deduction = pay/10;
        pay -= deduction;
        //cheks if deduction is larger or equal to the loan
        if(deduction >= loan) {
            //removes the loan and adds the remainder to pay
            deduction -= loan;
            loan = 0;
            pay += deduction;
            //hides loan html elements
            loanElement.style.display ="none";
            loanLabelElement.style.display = "none";
            repayElement.style.display = "none";
            //checks if a computer has been bought
            if(boughtKomputer) {
                //shows the loan button
                loanButtonElement.style.display = "block";
            }
        }
        else {
            //reduces the loan with the deduction amount
            loan -= deduction;
        }
    }
    //adds pay to balance and sets pay to 0
    balance += pay;
    pay = 0;
    //updates relevant html elements
    payElement.innerText = pay + "kr";
    balanceElement.innerText = balance + "kr";
    loanElement.innerText = loan + "kr";
}
//function for repaying loan button
function repayLoan() {
    //checks if pay is larger or equal to loan
    if(pay >= loan) {
        //lessens pay with loan and adds the remainder to balance
        pay -= loan;
        loan = 0;
        balance += pay;
        pay = 0;
        //updates the balance html element
        balanceElement.innerText = balance + "kr";
        //hides the loan info html elements
        loanElement.style.display ="none";
        loanLabelElement.style.display = "none";
        repayElement.style.display = "none";
        //cheks if a computer has been bought
        if(boughtKomputer) {
            //shows the loan button
            loanButtonElement.style.display = "block";
        }
    }
    else {
        //lessens loan with pay and sets pay to 0
        loan -= pay;
        pay = 0;
    }
    //updates html elements that changes in both if or else    
    payElement.innerText = pay + "kr";
    loanElement.innerText = loan + "kr";
    
}
//function for buy computer button
function buyComputer() {
    //checks if user can afford computer
    if(balance >= price) {
        //updates balance variable
        balance -= price;
        //updates balance html
        balanceElement.innerText = balance + "kr";
        //checks if the user has a loan
        if(loan == 0) {
            //shows the loan button if the user is able to get a new loan
            loanButtonElement.style.display = "block";
        }
        //sets intern variable to true
        boughtKomputer = true;
        //shows alert to user
        alert("You have bought a computer!!!");
        
    }
    //shows alert to user
    else {
        alert("You cannot afford this computer.")
    }
}